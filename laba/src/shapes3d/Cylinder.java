package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {

    public double height;

    public Cylinder(double radius, double height) {

        super(radius);
        this.height = height;

    }

    public double area() {

        return 2 * super.area() + height * 2 * Math.PI * radius;

    }

    public double volume() {

        return super.area() * height;

    }

    public String printer() {
        return "Cylinder ||  height = " + height + "  &&  radius = " + radius;
    }

}
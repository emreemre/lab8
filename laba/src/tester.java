import shapes2d.Circle;

import shapes2d.Square;

import shapes3d.Cube;

import shapes3d.Cylinder;


    public class tester {
        public static void main(String[] args) {


            Circle object1 = new Circle(4);
            System.out.println("Circle area : " + object1.area());
            System.out.println(object1.printer());


            Square object2 = new Square(5);
            System.out.println("Square area : " + object2.area());
            System.out.println(object2.printer());


            Cube object3 = new Cube(3);
            System.out.println("Cube area : " + object3.volume());
            System.out.println("Cube volume : " + object3.volume());
            System.out.println(object3.printer());


            Cylinder object4 = new Cylinder(2, 3);
            System.out.println("Cylinder area : " + object4.area());
            System.out.println("Cylinder volume : " + object4.volume());
            System.out.println(object4.printer());

        }
}
